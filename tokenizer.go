package fishline

import (
	"bufio"
	"errors"
	"strings"
	"unicode/utf8"

	"framagit.org/benjamin.vaudour/collection/v2"
	"framagit.org/benjamin.vaudour/shell/v2/scanner"
)

var (
	quotes             = collection.SliceToSet([]rune("'\""))
	escapes            = collection.SliceToSet([]rune("\\"))
	spaces             = collection.SliceToSet([]rune("\t\n\v\f\r"))
	errImcompleteToken = errors.New("Incomplete token")
)

func isSpace(s string) bool {
	for _, r := range s {
		if !spaces.Contains(r) {
			return false
		}
	}
	return true
}

func isQuote(s string) bool {
	r, _ := utf8.DecodeRuneInString(s)
	return quotes.Contains(r)
}

type spaceTk struct{}
type quoteTk struct{}
type tk struct{}

func (t quoteTk) splitText(data []byte, atEOF bool) (advance int, token []byte, err error) {
	var e rune
	var i int
	for i < len(data) {
		r, w := utf8.DecodeRune(data[i:])
		if escapes.Contains(e) {
			e = 0
		} else if escapes.Contains(r) {
			e = r
		} else if spaces.Contains(r) || quotes.Contains(r) {
			return i, data[:i], nil
		}
		i += w
	}
	if atEOF {
		return len(data), data, nil
	}
	return 0, nil, nil
}

func (t quoteTk) splitQuote(data []byte, atEOF bool) (advance int, token []byte, err error) {
	var e rune
	q, w := utf8.DecodeRune(data)
	i := w
	for i < len(data) {
		r, w := utf8.DecodeRune(data[i:])
		if escapes.Contains(e) {
			e = 0
		} else if escapes.Contains(r) {
			e = r
		} else if quotes.Contains(r) {
			if r == q {
				i += w
				return i, data[:i], nil
			}
		}
		i += w
	}
	if atEOF {
		return len(data), data, errImcompleteToken
	}
	return 0, nil, nil
}

func (t quoteTk) Split(data []byte, atEOF bool) (advance int, token []byte, err error) {
	r, _ := utf8.DecodeRune(data)
	if quotes.Contains(r) {
		return t.splitQuote(data, atEOF)
	}
	return t.splitText(data, atEOF)
}

func (t spaceTk) splitSpace(data []byte, atEOF bool) (advance int, token []byte, err error) {
	i := 0
	for i < len(data) {
		r, w := utf8.DecodeRune(data[i:])
		if !spaces.Contains(r) {
			return i, data[:i], nil
		}
		i += w
	}
	if atEOF {
		return len(data), data, nil
	}
	return 0, nil, nil
}

func (t spaceTk) splitText(data []byte, atEOF bool) (advance int, token []byte, err error) {
	var e, q rune
	i := 0
	for i < len(data) {
		r, w := utf8.DecodeRune(data[i:])
		if spaces.Contains(r) && !quotes.Contains(q) && !escapes.Contains(e) {
			return i, data[:i], nil
		}
		if escapes.Contains(e) {
			e = 0
		} else if escapes.Contains(r) {
			e = r
		} else if quotes.Contains(q) {
			if r == q {
				q = 0
			}
		} else if quotes.Contains(r) {
			q = r
		}
		i += w
	}
	if atEOF {
		if escapes.Contains(e) || quotes.Contains(q) {
			return len(data), data, errImcompleteToken
		}
		return len(data), data, nil
	}
	return 0, nil, nil
}

func (t spaceTk) Split(data []byte, atEOF bool) (advance int, token []byte, err error) {
	r, _ := utf8.DecodeRune(data)
	if spaces.Contains(r) {
		return t.splitSpace(data, atEOF)
	}
	return t.splitText(data, atEOF)
}

func (t tk) Split(data []byte, atEOF bool) (advance int, token []byte, err error) {
	// Skip leading spaces.
	start := 0
	for w := 0; start < len(data); start += w {
		var r rune
		r, w = utf8.DecodeRune(data[start:])
		if !spaces.Contains(r) {
			break
		}
	}

	var q, e rune

	// Scan until space, marking end of word.
	for w, i := 0, start; i < len(data); i += w {
		var r rune
		r, w = utf8.DecodeRune(data[i:])
		if spaces.Contains(r) && !quotes.Contains(q) && !escapes.Contains(e) {
			return i, data[:i], nil
		}
		if escapes.Contains(e) {
			e = 0
		} else if escapes.Contains(r) {
			e = r
		} else if quotes.Contains(q) {
			if q == r {
				q = 0
			}
		} else if quotes.Contains(r) {
			q = r
		}
	}

	// If we're at EOF, we have a final, non-empty, non-terminated word. Return it.
	if atEOF && len(data) > start {
		if escapes.Contains(e) || quotes.Contains(q) {
			return len(data), data[start:], errImcompleteToken
		}
		return len(data), data, nil
	}
	// Request more data.
	return 0, nil, nil
}

func newSpaceScanner(arg string) *bufio.Scanner {
	return scanner.NewScanner(strings.NewReader(arg), spaceTk{})
}

func newQuoteScanner(arg string) *bufio.Scanner {
	return scanner.NewScanner(strings.NewReader(arg), quoteTk{})
}

func newArgScammer(arg string) *bufio.Scanner {
	return scanner.NewScanner(strings.NewReader(arg), tk{})
}
