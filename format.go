package fishline

import (
	"regexp"
	"strings"

	"framagit.org/benjamin.vaudour/collection/v2"
	"framagit.org/benjamin.vaudour/strutil/style"
)

type WordType interface {
	Format(string) string
}

func format(text string, styles ...string) string {
	if len(styles) == 0 || len(text) == 0 {
		return text
	}
	return style.Format(text, styles...)
}

type wordType int

const (
	CommandType wordType = iota
	CommandErrorType
	OptionType
	OptionErrorType
	QuoteType
	QuoteErrorType
	NumberType
	TextType
	HistoryType
)

func (t wordType) Format(word string) string {
	var styles []string
	switch t {
	case CommandType:
		styles = []string{"l_green", "!bold"}
	case CommandErrorType:
		styles = []string{"red", "!bold"}
	case OptionType:
		styles = []string{"l_yellow", "!bold"}
	case OptionErrorType:
		styles = []string{"red", "!bold"}
	case QuoteType:
		styles = []string{"yellow", "!bold"}
	case QuoteErrorType:
		styles = []string{"red", "!bold"}
	case NumberType:
		styles = []string{"l_cyan", "!bold"}
	case HistoryType:
		styles = []string{"l_dark", "!bold"}
	default:
		var words []string
		sc := newQuoteScanner(word)
		for sc.Scan() {
			w := sc.Text()
			if isQuote(w) {
				w = QuoteType.Format(w)
			}
			words = append(words, w)
		}
		if sc.Err() == errImcompleteToken {
			words = append(words, QuoteErrorType.Format(sc.Text()))
		}
		return strings.Join(words, "")
	}
	return format(word, styles...)
}

var (
	CommandStyle      = []string{"l_green", "!bold"}
	CommandErrorStyle = []string{"red", "!bold"}
	OptionStyle       = []string{}
	OptionErrorStyle  = []string{}
	QuoteStyle        = []string{"yellow", "!bold"}
	QuoteErrorStyle   = []string{"l_red", "!bold"}
	NumberStyle       = []string{}
	TextStyle         = []string{}
	HistoryStyle      = []string{"l_dark", "!bold"}
)

type ParserFunc func([]string) []WordType

func Parse(commands []string) ParserFunc {
	return func(args []string) []WordType {
		out := make([]WordType, len(args))
		for i, e := range args {
			if i == 0 {
				if collection.Contains(commands, e) {
					out[i] = CommandType
				} else {
					out[i] = CommandErrorType
				}
			} else if regexp.MustCompile(`^(\+|-)?(\d+(\.\d*)?|\.\d+)$`).MatchString(e) {
				out[i] = NumberType
			} else if regexp.MustCompile(`^-(-)?[A-Za-z0-9]+$`).MatchString(e) {
				out[i] = OptionType
			} else {
				out[i] = TextType
			}
		}
		return out
	}
}

type FormatFunc func(string) string

func Format(commands []string) FormatFunc {
	return func(arg string) string {
		var words []string
		sc := newSpaceScanner(arg)
		for sc.Scan() {
			words = append(words, sc.Text())
		}
		if sc.Err() == errImcompleteToken {
			words = append(words, sc.Text())
		}
		var args []string
		m := make(map[int]int)
		for i, w := range words {
			if !isSpace(w) {
				m[len(args)] = i
				args = append(args, w)
			}
		}
		types := Parse(commands)(args)
		for i, t := range types {
			words[m[i]] = t.Format(args[i])
		}
		return strings.Join(words, "")
	}
}
