package fishline

type Autocompleter struct {
	Value   string
	Display string
}

//type FormatFunc func(input string, history string) string
type AutocompleteFunc func(arguments []string, pos int) []Autocompleter
