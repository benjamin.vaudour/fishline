package fishline

import (
	"bufio"
	"io"
)

type fishHistory struct {
	data   []string
	index  map[string]int
	cursor int
}

func isCursorValid(i, l int) bool {
	return i >= 0 && i < l
}

func scanData(data []byte, atEOF bool) (advance int, token []byte, err error) {
	if atEOF && len(data) == 0 {
		return 0, nil, nil
	}
	l := len(data)
	for i, b := range data {
		if b == '\n' && i < l-1 && data[i+1] == 3 { // EOT
			return i + 2, data[:i], nil
		}
	}
	if atEOF {
		return len(data), data, nil
	}
	return 0, nil, nil
}

func (fh *fishHistory) Len() int {
	return len(fh.data)
}

func (fh *fishHistory) Cursor() int {
	return fh.cursor
}

func (fh *fishHistory) Index(i int) string {
	l := fh.Len()
	if i < 0 {
		i += l
	}
	if isCursorValid(i, l) {
		return fh.data[i]
	}
	return ""
}

func (fh *fishHistory) SetCursor(c int) (ok bool) {
	l := fh.Len()
	if c < 0 {
		c += l
	}
	if ok = c >= -1 && c <= l; ok {
		fh.cursor = c
	}
	return
}

func (fh *fishHistory) Append(data string) {
	l := fh.Len()
	if i, exists := fh.index[data]; exists {
		if i < l-1 {
			newData := make([]string, l)
			copy(newData[:i], fh.data[:i])
			copy(newData[i:], fh.data[i+1:])
			newData[l-1] = data
			fh.data = newData
			fh.index[data] = l - 1
		}
	} else {
		fh.data = append(fh.data, data)
		fh.index[data] = l
	}
}

func (fh *fishHistory) Clear() {
	fh.data = fh.data[:0]
	fh.index = make(map[string]int)
	fh.cursor = 0
}

func (fh *fishHistory) Next() (ok bool) {
	if ok = fh.SetCursor(fh.cursor + 1); ok {
		ok = isCursorValid(fh.cursor, fh.Len())
	}
	return
}

func (fh *fishHistory) Prev() (ok bool) {
	if ok = fh.SetCursor(fh.cursor - 1); ok {
		ok = isCursorValid(fh.cursor, fh.Len())
	}
	return
}

func (fh *fishHistory) Read(r io.Reader) (n int, err error) {
	sc := bufio.NewScanner(r)
	sc.Split(scanData)
	for sc.Scan() {
		fh.Append(sc.Text())
		n++
	}
	return n, sc.Err()
}

func (fh *fishHistory) Write(w io.Writer) (n int, err error) {
	wb := bufio.NewWriter(w)
	for _, item := range fh.data {
		if _, err = wb.WriteString(item); err != nil {
			break
		}
		if err = wb.WriteByte('\n'); err != nil {
			break
		}
		if err = wb.WriteByte(3); err != nil { // EOT
			break
		}
		n++
	}
	return
}
